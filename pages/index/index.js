//index.js
//获取应用实例
let QR=require('../../utils/qrcode.js')
Page({
  
  /**
   * 页面的初始数据
   */
  data: {
    placeholder: 'wxp://f2f1bAMEbQAW_rTehqCKkhaRYpx4BgOA9Dov'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let size=this.setCanvasSize()
    let url = this.data.placeholder
    this.createQRcode(url,'mycanvas',size.w,size.h)
  },
  createQRcode(url,canvasId,canvasWidth,canvasHeight){
    QR.qrApi.draw(url, canvasId, canvasWidth, canvasHeight)

  },
  setCanvasSize(){
    let size={}
    //获取设备信息
    let res=wx.getSystemInfoSync()
    console.log(res)
    let scale=686/750
    let width=res.windowWidth*scale
    let height=width
    size.w=width
    size.h=height
    return size
  },
  formSumbit(e){
    let url = e.detail.value.url || this.data.placeholder
    wx.showToast({
      title: '生成中',
      icon:'loading',
      duration:2000
    })
    let that=this
    let timer=setTimeout(()=>{
      let size = that.setCanvasSize()
   
      that.createQRcode(url, 'mycanvas', size.w, size.h)
      wx.hideToast()
      clearTimeout(timer)
    },2000)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return{
    title:'生成二维码'
    }
  }
})